@extends('layouts.master')
@section('content')

<header>
    <div class="row d-flex align-items-center">

        <div class="col-md-2 text-center text-md-left">
            <img src="{{ asset('assets/images/logo.jpg') }}" alt="logo" width="100" height="100" class="d-none d-md-block">
        </div>

        <div class="col-md-10">
            <h1>Bienvenue sur xXFan2JardinXx</h1>
            <p class="lead">Un site dédié au passionés de dégustage d'herbe</p>
        </div>
    </div>
</header>

<div class="row">
    <div class="col-md-4">
        <div class="card-body text-center">
            <br> <br> <br>
            <a href="{{ url('listerArticles') }}" class="btn btn-primary">Consultez la liste de nos articles</a>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card-body text-center">
            <img src="{{ asset('assets/images/brouette-enfant.jpg') }}" alt="biblio" class="img-fluid">
        </div>
    </div>
</div>
