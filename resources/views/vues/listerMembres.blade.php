@extends('layouts.master')
@section('content')

    <div class="container">
        <div class="blanc">
            <h1>Liste des membres inscrits</h1>
        </div>
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Id du membre</th>
                <th>Nom</th>
                <th>Email</th>
                <th>Preferences ecologiques</th>
                <th>Historique de lecture</th>
                <th>Participation communautaire</th>
            </tr>
            </thead>
            @foreach($mesMembres as $membre)
                <tr>
                    <td> {{ $membre->id }}</td>
                    <td> {{ $membre->nom }}</td>
                    <td> {{ $membre->email }}</td>
                    <td> {{ $membre->preferences_ecologiques }}</td>
                    <td> {{ $membre->historique_lecture }}</td>
                    <td> {{ $membre->participation_communautaire }}</td>
                    <td style="text-align: center;">
                        <a href="{{ route('modifierMembre',$membre->id) }}" class="btn btn-primary"> Modifier</a>
                        <a href="{{ route('supprimerMembre',$membre->id) }}" class="btn btn-danger"> Supprimer</a>

                    </td>
                </tr>
            @endforeach
            <BR> <BR>
        </table>

        <a class="btn btn-primary" href="{{ url('ajouterMembre') }}">Ajouter un membre</a>

    </div>
