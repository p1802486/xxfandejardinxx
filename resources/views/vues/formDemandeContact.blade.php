@extends('layouts.master')
@section('content')
    <form action="{{ route('demanderContact.contactRequest') }}" method="POST">
        @csrf
        @method('PUT')
        <br><br>
        <br><br>
        <div class="container">
            <div class="blanc">
                <h1>Formulaire de contact</h1>
            </div>

            <div class="col-md-12 well well-sm">
                <div class="form-group">
                    <label class="col-md-3 control-label">Sujet : </label>
                    <div class="col-md-3">
                        <input id='titre' type="text" name="sujet" value="" class="form-control" required autofocus>
                    </div>
                </div>

                <BR> <BR>

                <div class="form-group">
                    <label class="col-md-3 control-label">Message : </label>
                    <div class="col-md-3">
                        <textarea value="" class="form-control" name="message" style="height:150px"></textarea>
                    </div>
                </div>

                <BR> <BR>
                <BR> <BR>
                <BR> <BR>
                <BR> <BR>

                <div class="form-group">
                    <label class="col-md-3 control-label">Votre nom: </label>
                    <div class="col-md-3">
                        <input id='nom' type="text" name="nom" value="" class="form-control" required autofocus>
                    </div>
                </div>

                <BR> <BR>

                <div class="form-group">
                    <label class="col-md-3 col-sm-3 control-label">Email : </label>
                    <div class="col-md-3">
                        <input id='categorie_eco' type="text" name="email" class="form-control" required autofocus>
                    </div>
                </div>


                <BR> <BR> <BR>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-3 col-sm-6 col-md-offset-3">

                        <button type="submit" class="btn btn-default btn-primary">
                            <span class="glyphicon glyphicon-ok"></span> Valider
                        </button>

                        <button type="button" class="btn btn-default btn-primary"
                                onclick="javascript:if(confirm('vous en etes sur ?')) window.location = '{{url('/')}}';">
                            <span class="glyphicon glyphicon-remove"></span> Annuler
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
