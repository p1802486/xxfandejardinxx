@extends('layouts.master')
@section('content')
    <form action="{{ route('ajouterArticle.ajoutArticle') }}" method="POST">
        @csrf
        @method('PUT')
        <br><br>
        <br><br>
        <div class="container">
            <div class="blanc">
                <h1>Ajouter un article</h1>
            </div>

            <div class="col-md-12 well well-sm">
                <div class="form-group">
                    <label class="col-md-3 control-label">Titre : </label>
                    <div class="col-md-3">
                        <input id='titre' type="text" name="titre" value="" class="form-control" required autofocus>
                    </div>
                </div>

                <BR> <BR>

                <div class="form-group">
                    <label class="col-md-3 control-label">Contenu : </label>
                    <div class="col-md-3">
                        <textarea value="" class="form-control" name="contenu" style="height:150px"></textarea>
                    </div>
                </div>

                <BR> <BR>
                <BR> <BR>
                <BR> <BR>
                <BR> <BR>

                <div class="form-group">
                    <label class="col-md-3 control-label">Auteur : </label>
                    <div class="col-md-3">
                        <input id='auteur' type="text" name="auteur" value="" class="form-control" required autofocus>
                    </div>
                </div>

                <BR> <BR>

                <div class="form-group">
                    <label class="col-md-3 col-sm-3 control-label">Catégorie écologique : </label>
                    <div class="col-md-3">
                        <input id='categorie_eco' type="text" name="categorie_ecologique" class="form-control" required autofocus>
                    </div>
                </div>

                <BR> <BR>

                <div class="form-group">
                    <label class="col-md-3 col-sm-3 control-label">Année de publication : </label>
                    <div class="col-md-2 col-sm-2">
                        <input type="date" id='date_pub' name="date_publication" class="form-control" required autofocus>
                    </div>
                </div>

                <BR> <BR> <BR>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-3 col-sm-6 col-md-offset-3">

                        <button type="submit" class="btn btn-default btn-primary">
                            <span class="glyphicon glyphicon-ok"></span> Valider
                        </button>

                        <button type="button" class="btn btn-default btn-primary"
                                onclick="javascript:if(confirm('vous en etes sur ?')) window.location = '{{url('/')}}';">
                            <span class="glyphicon glyphicon-remove"></span> Annuler
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
