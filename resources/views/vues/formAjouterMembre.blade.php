@extends('layouts.master')
@section('content')
    <form action="{{ route('ajouterMembre.ajoutMembre') }}" method="POST">
        @csrf
        @method('PUT')
        <br><br>
        <br><br>
        <div class="container">
            <div class="blanc">
                <h1>Ajouter un membre</h1>
            </div>

            <div class="col-md-12 well well-sm">
                <div class="form-group">
                    <label class="col-md-3 control-label">Nom : </label>
                    <div class="col-md-3">
                        <input id='nom' type="text" name="nom" value="" class="form-control" required autofocus>
                    </div>
                </div>

                <BR> <BR>

                <div class="form-group">
                    <label class="col-md-3 control-label">Email : </label>
                    <div class="col-md-3">
                        <input id='email' type="text" name="email" value="" class="form-control" required autofocus>
                    </div>
                </div>

                <BR> <BR>

                <div class="form-group">
                    <label class="col-md-3 control-label">Preferences ecologiques : </label>
                    <div class="col-md-3">
                        <textarea class="form-control" name="pref_eco" style="height:150px"></textarea>
                    </div>
                </div>

                <BR> <BR>
                <BR> <BR>
                <BR> <BR>
                <BR> <BR>

                <div class="form-group">
                    <label class="col-md-3 col-sm-3 control-label">Historique de lecture : </label>
                    <div class="col-md-3">
                        <textarea class="form-control" name="historique_lecture" style="height:150px"></textarea>
                    </div>
                </div>

                <BR> <BR>
                <BR> <BR>
                <BR> <BR>
                <BR> <BR>

                <div class="form-group">
                    <label class="col-md-3 col-sm-3 control-label">Participation communautaire : </label>
                    <div class="col-md-3">
                        <textarea class="form-control" name="part_comm" style="height:150px"></textarea>
                    </div>
                </div>

                <BR> <BR> <BR>
                <BR> <BR> <BR>
                <BR> <BR> <BR>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-3 col-sm-6 col-md-offset-3">

                        <button type="submit" class="btn btn-default btn-primary">
                            <span class="glyphicon glyphicon-ok"></span> Valider
                        </button>

                        <button type="button" class="btn btn-default btn-primary"
                                onclick="javascript:if(confirm('vous en etes sur ?')) window.location = '{{url('/')}}';">
                            <span class="glyphicon glyphicon-remove"></span> Annuler
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
