@extends('layouts.master')
@section('content')
    <form action="{{ route('updateArticle', $unArticle->id) }}" method="POST">
        @csrf
        @method('PUT')
        <br><br>
        <br><br>
        <div class="container">
            <div class="blanc">
                <h1>Modifier un article</h1>
            </div>

            <div class="col-md-12 well well-sm">
                <div class="form-group">
                    <label class="col-md-3 control-label">Titre : </label>
                    <div class="col-md-3">
                        <input type="hidden" name="id_article" value="{{ $unArticle->id ?? '' }}}">
                        <input type="text" name="titre" value="{{ $unArticle->titre ?? '' }}" class="form-control">
                    </div>
                </div>

                <BR> <BR>

                <div class="form-group">
                    <label class="col-md-3 control-label">Contenu : </label>
                    <div class="col-md-3">
                        <input type="hidden" name="id_article" value="{{ $unArticle->id ?? '' }}}">
                        <textarea class="form-control" name="contenu" style="height:150px">{{ $unArticle->contenu ?? '' }}</textarea>
                    </div>
                </div>

                <BR> <BR>
                <BR> <BR>
                <BR> <BR>
                <BR> <BR>

                <div class="form-group">
                    <label class="col-md-3 control-label">Auteur : </label>
                    <div class="col-md-3">
                        <input type="hidden" name="id_article" value="{{ $unArticle->id ?? '' }}}">
                        <input type="text" name="auteur" value="{{ $unArticle->auteur ?? '' }}" class="form-control">
                    </div>
                </div>

                <BR> <BR>

                <div class="form-group">
                    <label class="col-md-3 control-label">Catégorie écologique : </label>
                    <div class="col-md-3">
                        <input type="hidden" name="id_article" value="{{ $unArticle->id ?? '' }}}">
                        <input type="text" name="categorie_ecologique" value="{{ $unArticle->categorie_ecologique ?? '' }}" class="form-control">
                    </div>
                </div>

                <BR> <BR>

                <div class="form-group">
                    <label class="col-md-3 control-label">Année de publication : </label>
                    <div class="col-md-3">
                        <input type="hidden" name="id_article" value="{{ $unArticle->id ?? '' }}}">
                        <input type="date" name="annee_publication" value="{{ $unArticle->date_publication ?? '' }}" class="form-control">
                    </div>
                </div>

                <BR> <BR> <BR>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-3 col-sm-6 col-md-offset-3">

                        <button type="submit" class="btn btn-default btn-primary">
                            <span class="glyphicon glyphicon-ok"></span> Valider
                        </button>

                        <button type="button" class="btn btn-default btn-primary"
                                onclick="javascript:if(confirm('Etes vous sur ?')) window.location = '{{url('/')}}';">
                            <span class="glyphicon glyphicon-remove"></span> Annuler
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
