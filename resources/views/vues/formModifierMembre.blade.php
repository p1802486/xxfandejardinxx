@extends('layouts.master')
@section('content')
    <form action="{{ route('updateMembre', $unMembre->id) }}" method="POST">
        @csrf
        @method('PUT')
        <br><br>
        <br><br>
        <div class="container">
            <div class="blanc">
                <h1>Modifier un membre</h1>
            </div>

            <div class="col-md-12 well well-sm">
                <div class="form-group">
                    <label class="col-md-3 control-label">Nom : </label>
                    <div class="col-md-3">
                        <input type="hidden" name="id_membre" value="{{ $unMembre->id ?? '' }}}">
                        <input type="text" name="nom" value="{{ $unMembre->nom ?? '' }}" class="form-control">
                    </div>
                </div>

                <BR> <BR>

                <div class="form-group">
                    <label class="col-md-3 control-label">Email : </label>
                    <div class="col-md-3">
                        <input type="hidden" name="id_membre" value="{{ $unMembre->id ?? '' }}}">
                        <input type="text" name="email" value="{{ $unMembre->email ?? '' }}" class="form-control">
                    </div>
                </div>

                <BR> <BR>

                <div class="form-group">
                    <label class="col-md-3 control-label">Preferences ecologiques : </label>
                    <div class="col-md-3">
                        <input type="hidden" name="id_membre" value="{{ $unMembre->id ?? '' }}}">
                        <textarea class="form-control" name="pref_ecologique" style="height:150px">{{ $unMembre->preferences_ecologiques ?? '' }}</textarea>
                    </div>
                </div>

                <BR> <BR>
                <BR> <BR>
                <BR> <BR>
                <BR> <BR>

                <div class="form-group">
                    <label class="col-md-3 control-label">Historique de lecture : </label>
                    <div class="col-md-3">
                        <input type="hidden" name="id_membre" value="{{ $unMembre->id ?? '' }}}">
                        <textarea class="form-control" name="hist_ecologique" style="height:150px">{{ $unMembre->historique_lecture ?? '' }}</textarea>
                    </div>
                </div>

                <BR> <BR>
                <BR> <BR>
                <BR> <BR>
                <BR> <BR>

                <div class="form-group">
                    <label class="col-md-3 control-label">Participation communautaire : </label>
                    <div class="col-md-3">
                        <input type="hidden" name="id_membre" value="{{ $unMembre->id ?? '' }}}">
                        <textarea class="form-control" name="part_comm" style="height:150px">{{ $unMembre->participation_communautaire ?? '' }}</textarea>
                    </div>
                </div>

                <BR> <BR> <BR>
                <BR> <BR> <BR>
                <BR> <BR> <BR>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-3 col-sm-6 col-md-offset-3">

                        <button type="submit" class="btn btn-default btn-primary">
                            <span class="glyphicon glyphicon-ok"></span> Valider
                        </button>

                        <button type="button" class="btn btn-default btn-primary"
                                onclick="javascript:if(confirm('Etes vous sur ?')) window.location = '{{url('/')}}';">
                            <span class="glyphicon glyphicon-remove"></span> Annuler
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
