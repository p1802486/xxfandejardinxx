@extends('layouts.master')
@section('content')

    <div class="container">
        <div class="blanc">
            <h1>Liste des articles publiés</h1>
        </div>
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Id de l'article</th>
                <th>Titre</th>
                <th>Contenu</th>
                <th>Auteur</th>
                <th>Catégorie écologique</th>
                <th>Date de publication</th>
            </tr>
            </thead>
            @foreach($mesArticles as $article)
                <tr>
                    <td> {{ $article->id }}</td>
                    <td> {{ $article->titre }}</td>
                    <td> {{ $article->contenu }}</td>
                    <td> {{ $article->auteur }}</td>
                    <td> {{ $article->categorie_ecologique }}</td>
                    <td> {{ $article->date_publication }}</td>
                    <td style="text-align: center;">
                        <a href="{{ route('ajouterCommentaire', $article->id) }}" class="btn btn-success"> Commenter</a>
                        <a href="{{ route('modifierArticle', $article->id) }}" class="btn btn-primary"> Modifier</a>
                        <a href="{{ route('supprimerArticle', $article->id) }}" class="btn btn-danger"> Supprimer</a>
                    </td>
                </tr>
            @endforeach
            <BR> <BR>
        </table>

        <a class="btn btn-primary" href="{{ url('ajouterArticle') }}">Ajouter un article</a>
    </div>
