@extends('layouts.master')
@section('content')

<div class="container">
    <div class="blanc">
        <h1>Liste des commentaires publiés</h1>
    </div>
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Id du commentaire</th>
            <th>Nom de l'article commenté</th>
            <th>Auteur</th>
            <th>Commentaire</th>
            <th>Date du commentaire</th>
        </tr>
        </thead>
        @foreach($mesCommentaires as $commentaire)
            <tr>
                <td> {{ $commentaire->commId }}</td>
                <td> {{ $commentaire->articleTitle }}</td>
                <td> {{ $commentaire->membreNom }}</td>
                <td> {{ $commentaire->commentaire }}</td>
                <td> {{ $commentaire->date_commentaire }}</td>
                <td style="text-align: center;">
                    <a href="{{ route('supprimerCommentaire', $commentaire->commId) }}" class="btn btn-danger"> Supprimer</a>
                </td>
            </tr>
        @endforeach
        <BR> <BR>
    </table>

    <a class="btn btn-primary" href="{{ url('ajouterCommentaire', $commentaire->article_id) }}">Ajouter un commentaire</a>
</div>
