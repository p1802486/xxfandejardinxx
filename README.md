<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Comment lancer le projet

Il faut au préalable avoir installé XAMPP.
Une fois installé, il faut cloner le projet dans le dossier htdocs de Xampp.
Il faut donc le cloner dans le chemin suivant : "C:\xampp\htdocs"
<br> <br>
Une fois que le projet est cloné dans le chemin cité ci-dessus. Il faut lancer les modules 
Apache et MySQL dans XAMPP puis importer la base de donnée dans MySQL depuis le fichier "script_gen_bd.sql" présent dans la racine du projet xXFanDeJardinXx.
<br> <br>
Une fois la base importée dans MySQL. Vous pouvez lancer le projet Laravel avec la commande suivante dans un terminal lancée dans la racine du projet :
```
php artisan serve
```
Vous pouvez aussi utiliser un IDE comme PhpStorm ou un éditeur de texte comme Visual Studio Code.

Une fois lancé, le projet devrait être accessible à l'adresse suivante :
```
http://localhost/xxfandejardinxx/public/
```
