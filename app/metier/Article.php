<?php

namespace App\metier;

class Article extends Model {

    protected $table = 'articles';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'titre',
        'contenu',
        'auteur',
        'categorie_ecologique',
        'date_publication'
    ];
}
