<?php

namespace App\metier;

class Membre extends Model {

    protected $table = 'membres';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'nom',
        'email',
        'preferences_ecologiques',
        'historique_lecture',
        'participation_communautaire'
    ];
}
