<?php

namespace App\metier;

class Commentaire extends Model {

    protected $table = 'commentaires';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'titreArticle',
        'auteurCommentaire',
        'commentaire',
        'date_commentaire'
    ];
}
