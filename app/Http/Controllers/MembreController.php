<?php

namespace App\Http\Controllers;

use App\dao\ServiceMembre;
use App\DAO\ServiceOuvrage;
use App\DAO\ServiceReservation;
use App\Exceptions\MonException;
use App\dao\ServiceManga;
use App\dao\ServiceGenre;
use App\dao\ServiceDessinateur;
use App\dao\ServiceScenariste;

use Illuminate\support\facades\Input;
use Request;

class MembreController extends Controller {

    public function listerMembres() {
        try {
            $unServiceMembre = new ServiceMembre();
            $mesMembres = $unServiceMembre->listerMembres();
            return view('vues/listerMembres', compact('mesMembres'));
        } catch (MonException $e) {
            $monErreur = $e->getMessage();
            return view('vues/error', compact('monErreur'));
        }
    }

    public function getModifierMembreForm($id) {
        $unServiceMembre = new ServiceMembre();
        $unMembre = $unServiceMembre->getMembreById($id);
        return view('vues/formModifierMembre', compact('unMembre'));
    }

    public function updateMembre($id) {
        try {
            $nom = Request::get('nom');
            $email = Request::get('email');
            $preferences_ecologiques = Request::get('pref_ecologique');
            $historique_lecture = Request::get('hist_ecologique');
            $participation_communautaire = Request::get('part_comm');
            $unServiceMembre = new ServiceMembre();
            $unServiceMembre->updateMembre(
                $id,
                $nom,
                $email,
                $preferences_ecologiques,
                $historique_lecture,
                $participation_communautaire
            );
            return redirect('/listerMembres');
        } catch (\Illuminate\Database\MonException $e) {
            throw new MonException($e->getMessage(), 5);
        }
    }

    public function supprimerMembre($id) {
        $unServiceMembre = new ServiceMembre();
        $unServiceMembre->supprimerMembre($id);
        return redirect('/listerMembres');
    }

    public function ajouterMembre() {
        return view('vues/formAjouterMembre');
    }

    public function ajoutMembre() {
        try {
            $nom = Request::input('nom');
            $email = Request::input('email');
            $preferences_ecologiques = Request::input('pref_eco');
            $historique_lecture = Request::input('historique_lecture');
            $participation_communautaire = Request::input('part_comm');
            $unServiceMembre = new ServiceMembre();
            $unServiceMembre->ajouterMembre($nom, $email, $preferences_ecologiques, $historique_lecture, $participation_communautaire);
            return view('home');
        } catch(MonException $e) {
            $monErreur = $e->getMessage();
            return view('vues/error', compact('monErreur'));
        } catch(\Exception $ex) {
            $monErreur = $ex->getMessage();
            return view('vues/error', compact('monErreur'));
        }
    }
}
