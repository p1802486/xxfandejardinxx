<?php

namespace App\Http\Controllers;

use App\dao\ServiceContact;
use App\DAO\ServiceOuvrage;
use App\DAO\ServiceReservation;
use App\Exceptions\MonException;
use App\dao\ServiceManga;
use App\dao\ServiceGenre;
use App\dao\ServiceDessinateur;
use App\dao\ServiceScenariste;

use Illuminate\support\facades\Input;
use Request;

class ContactController extends Controller {

    public function demandeContact() {
        return view('vues/formDemandeContact');
    }

    public function contactRequest() {
        try {
            $sujet = Request::input('sujet');
            $message = Request::input('message');
            $nom = Request::input('nom');
            $email = Request::input('email');
            $unServiceContact = new ServiceContact();
            $unServiceContact->ajouterContact($sujet, $message, $nom, $email);
            return view('home');
        } catch(MonException $e) {
            $monErreur = $e->getMessage();
            return view('vues/error', compact('monErreur'));
        } catch(\Exception $ex) {
            $monErreur = $ex->getMessage();
            return view('vues/error', compact('monErreur'));
        }
    }
}
