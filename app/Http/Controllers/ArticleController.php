<?php

namespace App\Http\Controllers;

use App\dao\ServiceArticle;
use App\DAO\ServiceOuvrage;
use App\DAO\ServiceReservation;
use App\Exceptions\MonException;
use App\dao\ServiceManga;
use App\dao\ServiceGenre;
use App\dao\ServiceDessinateur;
use App\dao\ServiceScenariste;

use Illuminate\support\facades\Input;
use Request;

class ArticleController extends Controller {

    public function listerArticles() {
        try {
            $unServiceArticle = new ServiceArticle();
            $mesArticles = $unServiceArticle->getAllArticles();
            return view('vues/listerArticles', compact('mesArticles'));
        } catch (MonException $e) {
            $monErreur = $e->getMessage();
            return view('vues/error', compact('monErreur'));
        }
    }

    public function getModifierArticleForm($id) {
        $unServiceArticle = new ServiceArticle();
        $unArticle = $unServiceArticle->getArticleById($id);
        return view('vues/formModifierArticle', compact('unArticle'));
    }

    public function updateArticle($id) {
        try {
            $titre = Request::get('titre');
            $contenu = Request::get('contenu');
            $auteur = Request::get('auteur');
            $categorie_ecologique = Request::get('categorie_ecologique');
            $date_publication = Request::get('annee_publication');
            $unServiceArticle = new ServiceArticle();
            $unServiceArticle->updateArticle(
                $id,
                $titre,
                $auteur,
                $contenu,
                $categorie_ecologique,
                $date_publication
            );
            return redirect('/listerArticles');
        } catch (\Illuminate\Database\MonException $e) {
            throw new MonException($e->getMessage(), 5);
        }
    }

    public function supprimerArticle($id) {
        $unServiceArticle = new ServiceArticle();
        $unServiceArticle->supprimerArticle($id);
        return redirect('/listerArticles');
    }

    public function ajouterArticle() {
        return view('vues/formAjouterArticle');
    }

    public function ajoutArticle() {
        try {
            $titre = Request::input('titre');
            $contenu = Request::input('contenu');
            $auteur = Request::input('auteur');
            $categorie_ecologique = Request::input('categorie_ecologique');
            $date_publication = Request::input('date_publication');
            $unServiceArticle = new ServiceArticle();
            $unServiceArticle->ajouterArticle($titre, $contenu, $auteur, $categorie_ecologique, $date_publication);
            return view('home');
        } catch(MonException $e) {
            $monErreur = $e->getMessage();
            return view('vues/error', compact('monErreur'));
        } catch(\Exception $ex) {
            $monErreur = $ex->getMessage();
            return view('vues/error', compact('monErreur'));
        }
    }
}
