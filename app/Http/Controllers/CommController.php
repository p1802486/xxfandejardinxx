<?php

namespace App\Http\Controllers;

use App\dao\ServiceArticle;
use App\dao\ServiceCommentaire;
use App\dao\ServiceMembre;
use App\DAO\ServiceOuvrage;
use App\DAO\ServiceReservation;
use App\Exceptions\MonException;
use App\dao\ServiceManga;
use App\dao\ServiceGenre;
use App\dao\ServiceDessinateur;
use App\dao\ServiceScenariste;

use App\Models\User;
use Illuminate\support\facades\Input;
use Request;

class CommController extends Controller {

    public function listerComm() {
        try {
            $unServiceCommentaire = new ServiceCommentaire();
            $mesCommentaires = $unServiceCommentaire->getAllCommsWithNamesAndTitles();
            return view('vues/listerCommentaires', compact('mesCommentaires'));
        } catch (MonException $e) {
            $monErreur = $e->getMessage();
            return view('vues/error', compact('monErreur'));
        }
    }

    public function ajouterCommentaireForm($id) {
        $unServiceArticle = new ServiceArticle();
        $unArticle = $unServiceArticle->getArticleById($id);
        return view('vues/formAjouterCommentaire', compact('unArticle'));
    }

    public function ajoutCommentaire($idArticle) {
        try {
            $nomFromInput = Request::input('nom');
            $message = Request::input('message');

            $unServiceMembre = new ServiceMembre();
            $unMembre = $unServiceMembre->getMembreByName($nomFromInput);

            $unServiceCommentaire = new ServiceCommentaire();
            $unServiceCommentaire->ajoutCommentaire($idArticle, $unMembre->id, $message);
            return view('home');
        } catch(MonException $e) {
            $monErreur = $e->getMessage();
            return view('vues/error', compact('monErreur'));
        } catch(\Exception $ex) {
            $monErreur = $ex->getMessage();
            return view('vues/error', compact('monErreur'));
        }
    }

    public function supprimerCommentaire($commId) {
        $unServiceCommentaire = new ServiceCommentaire();
        $unServiceCommentaire->supprimerCommentaire($commId);
        return redirect('listerCommentaires');
    }
}
