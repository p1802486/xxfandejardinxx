<?php

namespace App\dao;
use Illuminate\Support\Facades\DB;
use App\Exceptions\MonException;

class ServiceCommentaire {

    public function getAllCommsWithNamesAndTitles() {
        try {
            $mesCommentaires = DB::table('commentaires')
                ->Select('commentaires.id as commId', 'articles.titre as articleTitle', 'membres.nom as membreNom', 'commentaires.commentaire', 'commentaires.date_commentaire', 'commentaires.article_id')
                ->join('articles', 'commentaires.article_id', '=', 'articles.id')
                ->join('membres', 'commentaires.membre_id', '=', 'membres.id')
                ->get();
            return $mesCommentaires;
        } catch (\Illuminate\Database\QueryException $e) {
            throw new MonException($e->getMessage());
        }
    }

    public function ajoutCommentaire(
        $idArticle,
        $idUser,
        $commentaire
    )  {
        try {
            DB::table('commentaires')
                ->insert(
                    [
                        'article_id' => $idArticle,
                        'membre_id' => $idUser,
                        'commentaire' => $commentaire,
                        'date_commentaire' => date('Y-m-d', time())
                    ]);
        } catch (\Illuminate\Database\QueryException $e) {
            throw new MonException($e->getMessage());
        }
    }

    public function supprimerCommentaire($commId) {
        try {
            $unCommentaire = DB::table('commentaires')
                ->where('id', $commId)
                ->first();

            if ($unCommentaire === null) {
                throw new MonException("No commentaires found with id: $commId");
            }

            DB::table('commentaires')
                ->where('id', $commId)
                ->delete();
        } catch (\Illuminate\Database\QueryException $e) {
            throw new MonException($e->getMessage());
        }
    }
}
