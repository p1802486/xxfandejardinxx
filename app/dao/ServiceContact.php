<?php

namespace App\dao;
use Illuminate\Support\Facades\DB;
use App\Exceptions\MonException;
use Illuminate\Database\QueryException;

class ServiceContact {

    public function ajouterContact(
        $sujet,
        $message,
        $nom,
        $email
    ) {
        try {
            DB::table('contact')->insert(
                [
                    'sujet' => $sujet,
                    'message' => $message,
                    'nom' => $nom,
                    'email' => $email,
                    'date_contact' => date('Y-m-d', time())
                ]);
        } catch (QueryException $e) {
            throw new MonException($e->getMessage(), 5);
        }
    }

}
