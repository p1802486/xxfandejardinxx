<?php

namespace App\dao;
use Illuminate\Support\Facades\DB;
use App\Exceptions\MonException;
use Illuminate\Database\QueryException;

class ServiceMembre {

    public function listerMembres() {
        try {
            $mesMembres = DB::table('membres')->get();
            return $mesMembres;
        } catch (\Illuminate\Database\QueryException $e) {
            throw new MonException($e->getMessage());
        }
    }

    public function getMembreByName($name) {
        try {
            $unMembre = DB::table('membres')
                ->select()
                ->where('membres.nom', '=', $name)
                ->first();

            return $unMembre;

        } catch (\Illuminate\Database\QueryException $e) {
            throw new MonException($e->getMessage());
        }
    }

    public function getMembreById($id) {
        try {
            $unMembre = DB::table('membres')
                ->select()
                ->where('membres.id', '=', $id)
                ->first();

            return $unMembre;

        } catch (\Illuminate\Database\QueryException $e) {
            throw new MonException($e->getMessage());
        }
    }

    public function updateMembre(
        $id,
        $nom,
        $email,
        $preferences_ecologiques,
        $historique_lecture,
        $participation_communautaire
    )
    {
        try {
            DB::table('membres')
                ->where('id', $id)
                ->update(
                    [
                        'nom' => $nom,
                        'email' => $email,
                        'preferences_ecologiques' => $preferences_ecologiques,
                        'historique_lecture' => $historique_lecture,
                        'participation_communautaire' => $participation_communautaire
                    ]);
        } catch (\Illuminate\Database\QueryException $e) {
            throw new MonException($e->getMessage());
        }
    }

    public function supprimerMembre($id) {
        try {
            $unMembre = DB::table('membres')
                ->where('id', $id)
                ->first();

            if ($unMembre === null) {
                throw new MonException("No reservation found with id: $id");
            }

            DB::table('commentaires')
                ->where('membre_id', $id)
                ->delete();

            DB::table('membres')
                ->where('id', $id)
                ->delete();
        } catch (\Illuminate\Database\QueryException $e) {
            throw new MonException($e->getMessage());
        }
    }

    public function ajouterMembre(
        $nom,
        $email,
        $preferences_ecologiques,
        $historique_lecture,
        $participation_communautaire
    ) {
        try {
            DB::table('membres')->insert(
                [
                    'nom' => $nom,
                    'email' => $email,
                    'preferences_ecologiques' => $preferences_ecologiques,
                    'historique_lecture' => $historique_lecture,
                    'participation_communautaire' => $participation_communautaire
                ]);
        } catch (QueryException $e) {
            throw new MonException($e->getMessage(), 5);
        }
    }
}
