<?php

namespace App\dao;
use Illuminate\Support\Facades\DB;
use App\Exceptions\MonException;
use Illuminate\Database\QueryException;

class ServiceArticle {

    public function getAllArticles() {
        try {
            $mesArticles = DB::table('articles')->get();
            return $mesArticles;
        } catch (\Illuminate\Database\QueryException $e) {
            throw new MonException($e->getMessage());
        }
    }

    public function getArticleById($id) {
        try {
            $unArticle = DB::table('articles')
                ->select()
                ->where('articles.id', '=', $id)
                ->first();

            return $unArticle;

        } catch (\Illuminate\Database\QueryException $e) {
            throw new MonException($e->getMessage());
        }
    }

    public function updateArticle(
        $id,
        $titre,
        $auteur,
        $contenu,
        $categorie_ecologique,
        $date_publication
    )
    {
        try {
            DB::table('articles')
                ->where('id', $id)
                ->update(
                    [
                        'titre' => $titre,
                        'auteur' => $auteur,
                        'contenu' => $contenu,
                        'categorie_ecologique' => $categorie_ecologique,
                        'date_publication' => $date_publication
                    ]);
        } catch (\Illuminate\Database\QueryException $e) {
            throw new MonException($e->getMessage());
        }
    }

    public function supprimerArticle($id) {
        try {
            $unArticle = DB::table('articles')
                ->where('id', $id)
                ->first();

            if ($unArticle === null) {
                throw new MonException("No reservation found with id: $id");
            }

            DB::table('commentaires')
                ->where('article_id', $id)
                ->delete();

            DB::table('articles')
                ->where('id', $id)
                ->delete();
        } catch (\Illuminate\Database\QueryException $e) {
            throw new MonException($e->getMessage());
        }
    }

    public function ajouterArticle(
        $titre,
        $contenu,
        $auteur,
        $categorie_ecologique,
        $date_publication
    ) {
        try {
            DB::table('articles')->insert(
                [
                    'titre' => $titre,
                    'contenu' => $contenu,
                    'auteur' => $auteur,
                    'categorie_ecologique' => $categorie_ecologique,
                    'date_publication' => $date_publication
                ]);
        } catch (QueryException $e) {
            throw new MonException($e->getMessage(), 5);
        }
    }
}
