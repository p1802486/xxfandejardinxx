-- Création de la base de données
DROP DATABASE IF EXISTS xxfandejardinxx;

CREATE DATABASE xxfandejardinxx;

-- Utilisation de la base de données
USE xxfandejardinxx;

-- Création de la table Articles
CREATE TABLE Articles (
    id INT AUTO_INCREMENT PRIMARY KEY,
    titre VARCHAR(100) NOT NULL,
    contenu VARCHAR(255) NOT NULL,
    auteur VARCHAR(100) NOT NULL,
    categorie_ecologique VARCHAR(255) NOT NULL,
    date_publication DATE NOT NULL
);

-- Création de la table Membres
CREATE TABLE Membres (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(100) NOT NULL,
    email VARCHAR(150) NOT NULL UNIQUE,
    preferences_ecologiques VARCHAR(100),
    historique_lecture VARCHAR(100),
    participation_communautaire VARCHAR(100),
    UNIQUE (nom),
    UNIQUE (email)
);

-- Création de la table Commentaires
CREATE TABLE Commentaires (
    id INT AUTO_INCREMENT PRIMARY KEY,
    article_id INT,
    membre_id INT,
    commentaire VARCHAR(255) NOT NULL,
    date_commentaire DATE NOT NULL,
    FOREIGN KEY (article_id) REFERENCES Articles(id),
    FOREIGN KEY (membre_id) REFERENCES Membres(id)
);

-- Création de la table Contact
CREATE TABLE Contact (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    sujet VARCHAR(255) NOT NULL,
    message VARCHAR(255) NOT NULL,
    date_contact DATE NOT NULL
);

-- Insertion des données dans la table Articles
INSERT INTO Articles (titre, contenu, auteur, categorie_ecologique, date_publication)
VALUES ('Les abeilles et leur importance écologique', 'Les abeilles jouent un rôle crucial dans la pollinisation...', 'Jean Dupont', 'Biodiversité', '2024-05-01'),
       ('Réduire son empreinte carbone', 'Voici quelques astuces pour réduire votre empreinte carbone...', 'Marie Curie', 'Changement Climatique', '2024-05-02');

-- Insertion des données dans la table Membres
INSERT INTO Membres (nom, email, preferences_ecologiques, historique_lecture, participation_communautaire)
VALUES ('Alice Martin', 'alice.martin@example.com', 'Biodiversité, Changement Climatique', 'Article 1, Article 2', 'Commenté sur Article 1'),
       ('Bob Dupuis', 'bob.dupuis@example.com', 'Énergie Renouvelable', 'Article 2', 'Participé à la discussion sur Article 2');

-- Insertion des données dans la table Commentaires
INSERT INTO Commentaires (article_id, membre_id, commentaire, date_commentaire)
VALUES (1, 1, 'Article très intéressant sur les abeilles!', '2024-05-03'),
       (2, 2, 'Merci pour ces conseils sur l\'empreinte carbone.', '2024-05-04');

-- Insertion des données dans la table Contact
INSERT INTO Contact (nom, email, sujet, message, date_contact)
VALUES ('Charles Baudelaire', 'charles.baudelaire@example.com', 'Problème de connexion', 'Je n\'arrive pas à me connecter à mon compte.', '2024-05-05'),
       ('Denise Rousseau', 'denise.rousseau@example.com', 'Suggestion d\'article', 'J\'aimerais voir un article sur la protection des océans.', '2024-05-06');
