<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('home');
});

/*
 * Routes liées à la gestion d'articles
 * */

Route::get('/listerArticles', [\App\Http\Controllers\ArticleController::class, 'listerArticles']);

Route::get('/modifierArticleForm/{id}', [\App\Http\Controllers\ArticleController::class, 'getModifierArticleForm'])->name("modifierArticle");

Route::put("/modifierArticle/{id}", [\App\Http\Controllers\ArticleController::class, 'updateArticle'])->name('updateArticle');

Route::get("/supprimerArticle/{id}", [\App\Http\Controllers\ArticleController::class, 'supprimerArticle'])->name('supprimerArticle');

Route::get('/ajouterArticle', [\App\Http\Controllers\ArticleController::class, 'ajouterArticle']);

Route::put('/ajoutArticle',
    [\App\Http\Controllers\ArticleController::class, 'ajoutArticle'])->name('ajouterArticle.ajoutArticle'
);

/*
 * Routes liées au formulaire de contact
 * */

Route::get('/contact', [\App\Http\Controllers\ContactController::class, 'demandeContact']);

Route::put('/demanderContact',
    [\App\Http\Controllers\ContactController::class, 'contactRequest'])->name('demanderContact.contactRequest'
);

/*
 * Routes liées à la gestion des commentaires
 * */

Route::get('/listerCommentaires', [\App\Http\Controllers\CommController::class, 'listerComm']);

Route::get("/supprimerCommentaire/{commId}", [\App\Http\Controllers\CommController::class, 'supprimerCommentaire'])->name('supprimerCommentaire');

Route::get('/ajouterCommentaire/{id}', [\App\Http\Controllers\CommController::class, 'ajouterCommentaireForm'])->name('ajouterCommentaire');

Route::put('/ajoutCommentaire/{id}',
    [\App\Http\Controllers\CommController::class, 'ajoutCommentaire'])->name('ajouterCommentaire.ajoutCommentaire'
);

/*
 * Routes liées à la gestion des membres
 * */

Route::get('/listerMembres', [\App\Http\Controllers\MembreController::class, 'listerMembres']);

Route::get('/modifierMembreForm/{id}', [\App\Http\Controllers\MembreController::class, 'getModifierMembreForm'])->name("modifierMembre");

Route::put("/modifierMembre/{id}", [\App\Http\Controllers\MembreController::class, 'updateMembre'])->name('updateMembre');

Route::get("/supprimerMembre/{id}", [\App\Http\Controllers\MembreController::class, 'supprimerMembre'])->name('supprimerMembre');

Route::get('/ajouterMembre', [\App\Http\Controllers\MembreController::class, 'ajouterMembre']);

Route::put('/ajoutMembre',
    [\App\Http\Controllers\MembreController::class, 'ajoutMembre'])->name('ajouterMembre.ajoutMembre'
);
